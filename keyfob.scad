include <arc.scad>

module keyfob(radius, thickness, fine) {
    union() {
        color("red")
            cylinder(r=radius, h=thickness, $fn=fine);
        color("orange")
            translate([0, radius, 0])
            arc(radius/2, thickness, fine);
        color("orange")
            translate([-radius/2, 0, thickness/2])
            rotate([-90, 0, 0])
            cylinder(r=thickness/2, h=radius, $fn=fine);
        color("orange")
            translate([+radius/2, 0, thickness/2])
            rotate([-90, 0, 0])
            cylinder(r=thickness/2, h=radius, $fn=fine);
    }
}

module char(ch, size, height) {
    linear_extrude(height)
        text(
            ch,
            font="Latin Modern Sans Demi Cond:style=Bold",
            //font="Liberation Sans Narrow:style=Bold",
            size=size,
            halign="center",
            valign="center"
        );
}

module ksc_keyfob() {
    radius = 20;
    thickness = 4;
    relief = 2;
    fine = 100;
    union() {
        color("blue") keyfob(radius, thickness, fine);
        translate([-0.5*radius, -0.15*radius, thickness])
            color("white")
            char("K", 0.7*radius, relief);
        translate([0, 0, thickness])
            color("white")
            char("S", 0.7*radius, relief);
        translate([+0.5*radius, +0.15*radius, thickness])
            color("white")
            char("C", 0.7*radius, relief);
        translate([0, 0, thickness])
            color("white")
            difference() {
                cylinder(r=0.95*radius, h=relief, $fn=fine);
                cylinder(r=0.90*radius, h=relief, $fn=fine);
            }
    }
}

ksc_keyfob();
