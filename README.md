# Tinkering with OpenSCAD

Just some tinkering with OpenSCAD. It's so awesome that these CAD designs are
just source code!

## Links

OpenSCAD:
 * [OpenSCAD Homepage](http://www.openscad.org/)
 * [Cheat Sheet](http://www.openscad.org/cheatsheet/index.html)
 * [User Manual](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual)

Vim plugin for OpenSCAD:
 * [Vim syntax highlighting](https://www.vim.org/scripts/script.php?script_id=3556)
