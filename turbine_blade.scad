//
// Turbine blade.
//

module turbine_blade(r_inner, r_outer, thickness, alpha) {
    // inner and outer radius, thickness of blade, width (as angle alpha,
    // 36 deg means 1/10 of the radius).
    inner_width_2 = r_inner * tan(alpha/2);
    outer_width_2 = r_outer * tan(alpha/2);
    points3d = [
        [r_inner, inner_width_2, 0],
        [r_outer, outer_width_2, 0],
        [r_outer, -outer_width_2, 0],
        [r_inner, -inner_width_2, 0],
        [r_inner, inner_width_2, thickness],
        [r_outer, outer_width_2, thickness],
        [r_outer, -outer_width_2, thickness],
        [r_inner, -inner_width_2, thickness],
    ];
    faces3d = [
        [3, 2, 1, 0],
        [1, 5, 4, 0],
        [2, 6, 5, 1],
        [7, 3, 0, 4],
        [7, 6, 2, 3],
        [4, 5, 6, 7],
    ];
    rotate([90, 0, 0])
        polyhedron(points3d, faces3d);
}

module turbine(r_inner, r_outer, thickness, n_blades, pitch) {
    // inner and outer radius, thickness of blade, numberof blades, pitch.
    delta_alpha = 360 / n_blades;
    axis_height = cos(pitch) * r_outer * tan(delta_alpha/2);
    union() {
        for (i = [0 : n_blades-1]) {
            rotate([0, i * delta_alpha, 0])
                rotate([pitch, 0, 0])
                    turbine_blade(r_inner, r_outer, thickness, delta_alpha);
        }
        rotate([90, 0, 0])
        cylinder(r=r_inner, h=axis_height, center=true, $fn=200);
        rotate([90, 0, 0])
        difference() {
            cylinder(r=r_outer+thickness, h=axis_height, center=true, $fn=200);
            cylinder(r=r_outer-thickness, h=axis_height, center=true, $fn=200);
        }
    }
}

module turbine_housing(r_inner, r_outer, height, spoke_width) {
    difference() {
        cylinder(r=r_outer, h=height, center=true, $fn=200);
        cylinder(r=r_inner, h=height, center=true, $fn=200);
    }
    translate([-r_outer, spoke_width/2, 0])
        color("orange")
        cube([110, 5, 5]);
}

//turbine_housing(51, 55, 10, 3);
rotate([90, 0, 0])
    turbine(10, 30, 1, 45, 25);

//rotate([0, 0, 0])
//    color("red")
//    rotate([15, 0, 0])
//    turbine_blade(10, 150, 1, 25);
//rotate([0, 90, 0])
//    color("yellow")
//    turbine_blade(10, 150, 1, 45);
//rotate([0, 180, 0])
//    color("blue")
//    turbine_blade(10, 150, 1, 45);
//rotate([0, 270, 0])
//    color("green")
//    turbine_blade(10, 150, 1, 45);
