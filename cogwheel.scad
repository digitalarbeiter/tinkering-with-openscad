// Cog Wheel.
// ----------
// cogwheel(r, cog_size, thickness)
// cogwheel_hole(r, cog_size, r_inner, thickness)
//
// r = (outer) radius of cog wheel (i.e. including cogs) in mm.
// cog_size = cog base width in mm; equals space between cogs; cog height
//            is twice the cog size.
// thickness = thickness of cog wheel in mm.
// r_inner = radius of space for axis (cogwheel_hole only).

use <polygon3d.scad>;


module cog(base_width, thickness) {
    // single cog of a cogwheel, with fixed form factor
    // (height = 2 * base width)
    top_height = 2.0 * base_width;
    top_width = 0.5 * base_width;
    flank_width = 0.1 * base_width;
    flank_height = 0.8 * top_height;
    polygon3d([
        [base_width/2, 0],
        [-base_width/2, 0],
        [-base_width/2+flank_width, flank_height],
        [-top_width/2, top_height],
        [+top_width/2, top_height],
        [+base_width/2-flank_width, flank_height],
    ], thickness);
}


module cogwheel(r, cog_size, thickness) {
    // cog wheel with (outer) radius, cog size = cog base width.
    approx_inner_r = r - 2 * cog_size;
    cog_n = floor(2 * 3.14159 * approx_inner_r / cog_size / 2);
    inner_u = 2 * cog_n * cog_size;
    inner_r = inner_u / (2*3.14159);
    union() {
        for (i = [0 : cog_n-1]) {
            rotate(360.0 / cog_n * i)
                translate([0.99 * inner_r, 0, 0])
                    rotate(-90)
                        cog(cog_size, thickness);
        }
        cylinder(h=thickness, r=inner_r, center=false, $fn=200);
    }
}


module cogwheel_hole(r, cog_size, r_inner, thickness) {
    // a cog wheel with a hole (of inner radius) for an axis
    difference() {
        cogwheel(r, cog_size, thickness);
        cylinder(h=thickness, r=r_inner, center=false);
    }
}
