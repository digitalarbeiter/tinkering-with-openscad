include <arc.scad>

module hollow_sphere(radius, thickness, fine) {
    difference() {
        sphere(r=radius, $fn=fine);
        sphere(r=radius-thickness, $fn=fine);
    }
}

module globe_base(radius, height, thickness, fine) {
    cylinder(r=radius, h=height, $fn=fine);
    translate([0, 0, 0.5*height])
        cylinder(r=thickness, h=3*height, $fn=fine);
}

module globe(radius, height, thickness, fine) {
    union() {
        globe_base(0.5*radius, height, 3*thickness, fine);
        translate([0, 0, radius + 3*height])
            color("red")
            hollow_sphere(radius, thickness, fine);
        translate([thickness, 0, 3*height+radius])
            color("green")
            rotate([-23, 0, 0])
            rotate([0, -90, 0])
            arc(radius+0.5*thickness, 2*thickness, 3*fine);
    }
}

globe(25, 2, 1, 20);
