// tiny model of a cupboard

module door(h, w, thickness) {
    // board
    translate([1.1*thickness, 0, 0])
        cube([h-2.2*thickness, (w/2)-1.6*thickness, thickness]);
    // hubs
    translate([0.1*thickness, 0.5*thickness, 0.5*thickness])
        rotate([0, 90, 0])
        color("red")
        cylinder(r=thickness/2, h=thickness, center=false, $fn=200);
    translate([h-1.1*thickness, 0.5*thickness, 0.5*thickness])
        rotate([0, 90, 0])
        color("red")
        cylinder(r=thickness/2, h=thickness, center=false, $fn=200);
    // nob
    nobwidth = 0.1*w;
    translate([(h-thickness)/2, (w/2)-1.6*thickness-nobwidth, thickness])
        color("blue")
        cube([thickness, nobwidth, 3*thickness]);
}

module body(h, w, d, thickness) {
    rotate([90, 0, 0]) {  // lay on back
        difference() {
            // body itself
            union() {
                translate([0, 0, 0]) cube([h, d, thickness]);
                translate([0, 0, w-thickness]) cube([h, d, thickness]);
                translate([0, 0, 0]) cube([thickness, d, w]);
                translate([h-thickness, 0, 0]) cube([thickness, d, w]);
                translate([0, 0, 0]) cube([h, thickness, w]);
            }
            // holes for hubs
            translate([0, d-2*thickness, 2.5*thickness])
                rotate([0, 90, 0])
                cylinder(r=(thickness/2)+0.3, h=thickness, center=false, $fn=200);
            translate([0, d-2*thickness, w-2.5*thickness])
                rotate([0, 90, 0])
                cylinder(r=(thickness/2)+0.3, h=thickness, center=false, $fn=200);
            translate([h-thickness, d-2*thickness, 2.5*thickness])
                rotate([0, 90, 0])
                cylinder(r=(thickness/2)+0.3, h=thickness, center=false, $fn=200);
            translate([h-thickness, d-2*thickness, w-2.5*thickness])
                rotate([0, 90, 0])
                cylinder(r=(thickness/2)+0.3, h=thickness, center=false, $fn=200);
        }
    }
}

module cupboard(h, w, d, thickness) {
    body(h, w, d, thickness);
    translate([0, 0.1*w, 0]) door(h, w, thickness);
    translate([0, 0.7*w, 0]) door(h, w, thickness);
}

cupboard(100, 50, 25, 1);
