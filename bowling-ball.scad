module ball(r, thickness, fine=20) {
    difference() {
        sphere(r=r, $fn=fine);
        sphere(r=r-thickness, $fn=fine);
    }
}

module bowling_ball(r, r_finger, thickness, fine=20) {
    difference() {
        ball(r, thickness, fine);
        rotate([0, 0, 0])
        cylinder(r=r_finger, h=r+thickness, $fn=fine);
        rotate([0, 23, 0])
        cylinder(r=r_finger, h=r+thickness, $fn=fine);
        rotate([20, 10, 0])
        cylinder(r=r_finger, h=r+thickness, $fn=fine);
    }
}

bowling_ball(25, 4, 4, 50);
