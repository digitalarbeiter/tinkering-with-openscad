
module arc(radius, thickness, fine) {
    steps = fine; //floor(6.3*(radius+thickness)/thickness);
    angle = 180/steps;
    translate([0, 0, thickness/2])
    rotate([0, 90, 90])
    union() {
        for (i = [0:steps]) {
            rotate([i*angle, 0, 0])
            translate([0, radius, 0])
            cylinder(r=thickness/2, h=thickness, $fn=fine);
        }
    }
}

