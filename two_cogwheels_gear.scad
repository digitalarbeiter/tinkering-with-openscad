use <cogwheel.scad>;

//
// project specific: gear base plate and covers, two actual cogwheels.
//

module gear_baseplate() {
    cube([110, 20, 2]);
    translate([0, 0, 2]) cube([2, 20, 6]);
    translate([108, 0, 2]) cube([2, 20, 6]);
    translate([30, 10, 2]) cylinder(h=8, r=4.9, center=false, $fn=200);
    translate([73, 10, 2]) cylinder(h=8, r=4.9, center=false, $fn=200);
}

module gear_cover() {
    difference() {
        cube([110, 20, 2]);
        translate([30, 10, 0]) cylinder(h=2, r=4.9, center=false, $fn=200);
        translate([73, 10, 0]) cylinder(h=2, r=4.9, center=false, $fn=200);
    }
}

gear_baseplate();
translate([0, 0, 8])
    gear_cover();
translate([30, 10, 2.5])
    cogwheel_hole(20, 4, 6, 5);
translate([73, 10, 2.5])
    cogwheel_hole(30, 4, 6, 5);
