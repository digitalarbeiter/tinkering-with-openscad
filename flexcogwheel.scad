// Flexible Cog Wheel.
// -------------------
// Cogwheel with configurable cogs.

use <polygon3d.scad>;


// some example cogs.

module tooth_cog(base_width, thickness) {
    // single cog of a cogwheel, with fixed form factor
    // (height = 2 * base width)
    top_height = 2.0 * base_width;
    top_width = 0.5 * base_width;
    flank_width = 0.1 * base_width;
    flank_height = 0.8 * top_height;
    rotate(-90)
        polygon3d([
            [base_width/2, 0],
            [-base_width/2, 0],
            [-base_width/2+flank_width, flank_height],
            [-top_width/2, top_height],
            [+top_width/2, top_height],
            [+base_width/2-flank_width, flank_height],
        ], thickness);
}

module ball_cog(width, height, thickness) {
    translate([0, 0, thickness/2]) {
        rotate([0, 90, 0]) cylinder(h=height, r=width/4, center=false, $fn=200);
        translate([height, 0, 0]) sphere(r=width/2, $fn=200);
    }
}


module flex_cogwheel(r, cog_height, cog_base_width, thickness) {
    // cogwheel with configurable cogs: make the cog a child of this module:
    //
    //     flex_cogwheel(r, cog_height, cog_base_width, thickness)
    //         my_cog(cog_height, cog_base_width, thickness);
    //
    // r = outer radius, including the cogs! for this we need cog height;
    // coge base width is needed for number of cogs.
    approx_inner_r = r - cog_height;
    cog_n = floor(2 * 3.14159 * approx_inner_r / cog_base_width / 2);
    inner_u = 2 * cog_n * cog_base_width;
    inner_r = inner_u / (2*3.14159);
    union() {
        for (i = [0 : cog_n-1]) {
            rotate(360.0 / cog_n * i)
                translate([0.99 * inner_r, 0, 0])
                    children();
        }
        cylinder(h=thickness, r=inner_r, center=false, $fn=200);
    }
}


translate([0, 0, 0]) color("blue")
    flex_cogwheel(30, 2, 2, 5) cube([2, 2, 5]);
translate([0, 0, 10]) color("yellow")
    flex_cogwheel(30, 4, 2, 5) tooth_cog(2, 5);
translate([0, 0, 20]) color("red")
    flex_cogwheel(30, 4, 2, 5) ball_cog(2, 4, 5);
