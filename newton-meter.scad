include <arc.scad>

module pipe(r_i, r_o, h, fine=20) {
    difference() {
        cylinder(r=r_o, h=h, $fn=fine);
        cylinder(r=r_i, h=h, $fn=fine);
    }
}

module casing(fine=20) {
    union() {
        difference() {
            pipe(15, 20, 100, fine=fine);
            translate([10, -5, 10])
            cube([15, 10, 80]);
        };
        // cap
        color("red")
        translate([0, 0, 100])
        cylinder(r=20, h=10, $fn=fine);
        // outer hook
        translate([-3, -3, 110])
        cube([6, 6, 25]);
        translate([-3, 0, 145])
        rotate([0, 90, 0])
        difference() {
            cylinder(r=15, h=6, $fn=fine);
            cylinder(r=10, h=6, $fn=fine);
            cube([20, 20, 6]);
        };
        // inner hook
        translate([-2, -2, 90])
        cube([4, 4, 10]);
        translate([-2, 0, 86])
        rotate([0, -90, 0])
        difference() {
            cylinder(r=10, h=4, $fn=fine);
            cylinder(r=6, h=4, $fn=fine);
            cube([10, 10, 4]);
        }
    }
}

module hook() {
    fine = 20;
    h = 10;
    union() {
        cylinder(r=14.5, h=h, $fn=fine);
        // upper hook
        translate([-2, -2, h])
        cube([4, 4, 10]);
        translate([-2, 0, h+16])
        rotate([0, 90, 0])
        difference() {
            cylinder(r=10, h=4, $fn=fine);
            cylinder(r=6, h=4, $fn=fine);
            cube([10, 10, 4]);
        }
        // lower hook
        translate([-2, -2, -10])
        cube([4, 4, 10]);
        translate([2, 0, -16])
        rotate([0, -90, 0])
        difference() {
            cylinder(r=10, h=4, $fn=fine);
            cylinder(r=6, h=4, $fn=fine);
            cube([10, 10, 4]);
        }
    }
}

//casing();
hook();
