// 3D Polygon.
// -----------
// Just for learning OpenSCAD, basically my own linear_extrude(){polygon()}.


module polygon3d(points, thickness) {
    // a polygon with a thickness, kinda like:
    // linear_extrude(height=thickness) { polygon(points); }
    N = len(points);
    points_bottom = [
        for (p = points)
        [p[0], p[1], 0]
    ];
    points_top = [
        for (p = points)
        [p[0], p[1], thickness]
    ];
    points3d = concat(points_bottom, points_top);
    upper_lower_faces = [
        [for (i = [N-1:-1:0]) i],
        [for (i = [0:N-1]) i+N],
    ];
    side_faces = [
        for (i = [0:N-2]) [i, i+1, i+N+1, i+N]
    ];
    last_face = [
        [0, N, 2*N-1, N-1],
    ];
    faces3d = concat(upper_lower_faces, side_faces, last_face);
    polyhedron(points3d, faces3d);
}
